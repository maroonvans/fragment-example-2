package dawitelias.com.fragmentexample2;

import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class FragmentExample2 extends ActionBarActivity implements FragmentOne
        .OnFragmentOneInteractionListener, FragmentTwo.OnFragmentTwoInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_example2);
        if (savedInstanceState == null) {
            FragmentOne frag1 = FragmentOne.newInstance("String 1", "String 2");
            FragmentTwo frag2 = FragmentTwo.newInstance("String 1", "String 2");

            // added both fragments to fragment manager
            // detach fragment 2 so its not visible but still exists
            // won't have to recreate fragment 2
            getFragmentManager().beginTransaction()
                    .add(R.id.the_info, frag2, "frag2")
                    .detach(frag2)
                    .add(R.id.the_info, frag1, "frag1")
                    .commit();
        }
        Button button1 = (Button)findViewById(R.id.button);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get the 2 fragments
                // detach 1 and attach the other
                FragmentOne frag1 = (FragmentOne)getFragmentManager().findFragmentByTag("frag1");
                FragmentTwo frag2 = (FragmentTwo)getFragmentManager().findFragmentByTag("frag2");

                FragmentTransaction ft = getFragmentManager().beginTransaction();

                if (frag2.isDetached()) { // and fragment 1 is attached
                    ft.detach(frag1).attach(frag2);
                }
                if (frag1.isDetached()) {
                    ft.detach(frag2).attach(frag1);
                }
                ft.commit();
            } // close onClick
        };

        button1.setOnClickListener(listener);
    } // onCreate


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fragment_example2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentOneInteraction(View v) {
        Context context = getApplicationContext();
        CharSequence text = "Hello from Fragment One";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    @Override
    public void onFragmentTwoInteraction(View v) {
        Context context = getApplicationContext();
        CharSequence text = "Hello from Fragment Two";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
